﻿#include <iostream>
#include "Tree.h"

int main()
{
	Tree myTree;
	myTree.FillTree();

	std::cout << "Minimum element in the tree: " << myTree.FindMin() << "\n";
	std::cout << "Maximum element in the tree: " << myTree.FindMax() << "\n";
	std::cout << "Sum of all elements in the tree: " << myTree.CalculateSum() << "\n";
	std::cout << "Number of elements in the tree: " << myTree.Size() << "\n";

	return 0;
}
