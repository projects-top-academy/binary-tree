#pragma once
#include "TreeNode.h"
#include <iostream>

class Tree {
private:
    TreeNode* root;

    TreeNode* InsertRecursive(TreeNode* current, int value) {
        if (current == nullptr) {
            return new TreeNode(value);
        }

        if (value > current->data) {
            current->right = InsertRecursive(current->right, value);
        }
        else {
            current->left = InsertRecursive(current->left, value);
        }

        return current;
    }

    int FindMinRecursive(TreeNode* current) {
        if (current->left == nullptr) {
            return current->data;
        }
        return FindMinRecursive(current->left);
    }

    int FindMaxRecursive(TreeNode* current) {
        if (current->right == nullptr) {
            return current->data;
        }
        return FindMaxRecursive(current->right);
    }

    int CalculateSumRecursive(TreeNode* current) {
        if (current == nullptr) {
            return 0;
        }
        return current->data + CalculateSumRecursive(current->left) + CalculateSumRecursive(current->right);
    }

    int CalculateSizeRecursive(TreeNode* current) {
        if (current == nullptr) {
            return 0;
        }
        return 1 + CalculateSizeRecursive(current->left) + CalculateSizeRecursive(current->right);
    }

public:
    Tree() : root(nullptr) {}

    void Insert(int value) {
        root = InsertRecursive(root, value);
    }

    void FillTree() {
        Insert(5);
        Insert(3);
        Insert(7);
        Insert(2);
        Insert(4);
        Insert(6);
        Insert(8);
    }

    int FindMin() {
        if (root == nullptr) {
            return -1;
        }
        return FindMinRecursive(root);
    }

    int FindMax() {
        if (root == nullptr) {
            return -1;
        }
        return FindMaxRecursive(root);
    }

    int CalculateSum() {
        return CalculateSumRecursive(root);
    }

    int Size() {
        return CalculateSizeRecursive(root);
    }
};

